package com.example.table;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText ID, NAME, NOTE;
    private Button btnSave;
    private ListView listItem;
    private ArrayAdapter adapterList;
    private ArrayList arrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ID = (EditText) findViewById(R.id.ID);
        NAME = (EditText) findViewById(R.id.NAME);
        NOTE = (EditText) findViewById(R.id.NOTE);
        btnSave = (Button) findViewById(R.id.btnSave);
        listItem = (ListView) findViewById(R.id.listItem);

        arrayList = new ArrayList<String>();
        adapterList = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,arrayList);
        listItem.setAdapter(adapterList);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Information;
                Information = "ID: "+ ID.getText().toString();
                Information = Information + "\nName: " + NAME.getText().toString();
                Information = Information + "\nNOTE: " + NOTE.getText().toString();

                if(!ID.getText().toString().trim().equals("") && !NAME.getText().toString().trim().equals("")){
                    arrayList.add(Information);

                    adapterList.notifyDataSetChanged();
                    ID.setText("");
                    NAME.setText("");
                    NAME.requestFocus();
                    NOTE.setText("");
                }
            }
        });
        //
        listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int i, long l) {
                Log.d(TAG, "onItemClick: "+ arrayList.get(i));




                EditTable editTable = new EditTable(MainActivity.this);
                editTable.show();
            }
        });

        //
        listItem.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                String Information;
                Information = "You just delete Information";
                Information = Information + arrayList.get(i);
                arrayList.remove(i);
                adapterList.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, Information, Toast.LENGTH_SHORT).show();
                return false;
            }
        });


    }
}