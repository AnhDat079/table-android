package com.example.table;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.ArrayList;


public class EditTable extends Dialog implements View.OnClickListener {
    private Context context;
    String ID, NAME, NOTE;
    Button SAVE;
    private ArrayAdapter adapterList;
    private ArrayList arrayList;

    public EditTable(@NonNull MainActivity context) {
        super(context);
        this.context = context;

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.edit_table);

        findViewById(R.id.btnSave).setOnClickListener(this);
        findViewById(R.id.btnCancel).setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSave:
                Toast.makeText(context,"Information has been changed",Toast.LENGTH_SHORT).show();

                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }
}
